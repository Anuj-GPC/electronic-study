Login = {
  Init: function() {},

  AdminLogin: function() {
    var username = $("#username").val();
    var password = $("#password").val();

    var input = { username: username, password: password };

    var postbackUrl = "../src/controller/logincontroller.php/Login";
    __DoAsyncPostBack(
      input,
      postbackUrl,
      Login.AdminLoginCallBack,
      "error.html"
    );
  },

  AdminLoginCallBack: function(AjaxOut) {
    if (AjaxOut.Success) {
      window.location.href = "course.html";
    } else {
      alert("Please try again!");
    }
  }
};
