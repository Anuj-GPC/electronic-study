Course = {
  Init: function() {
    Course.ListOfCourse();
  },

  RedirectToAddCoursePage: function() {
    window.location.href = "addcourse.html";
  },

  ListOfCourse: function() {
    var input = {};

    var postbackUrl = "../src/controller/coursecontroller.php/ListOfCourse";
    __DoAsyncPostBack(
      input,
      postbackUrl,
      Course.ListOfCourseCallBack,
      "error.html"
    );
  },

  ListOfCourseCallBack: function(AjaxOut) {
    if (AjaxOut.Success) {
      var i;
      var Count = AjaxOut.Result.RowCount;
      var BindHtml = "";
      for (i = 0; i < Count; i++) {
        BindHtml =
          BindHtml +
          "<tr>" +
          "<td>" +
          parseInt(i + 1) +
          "</td>" +
          "<td>" +
          AjaxOut.Result.Data[i].Name +
          "</td>" +
          '<td class="text-primary">' +
          '<a href="addcourse.html?cid='+ AjaxOut.Result.Data[i].ID +'"><i class="fa fa-edit"></i></a>  <a href="#" onclick="Course.DeleteCourse('+ AjaxOut.Result.Data[i].ID +')"><i class="fa fa-trash-o"></i></a>' +
          "</td>" +
          "</tr>";
      }
      $("#CourseDisplay").html(BindHtml);
    } else {
    }
  },
	  
  DeleteCourse: function(cid) {

if(confirm ("Are you sure to delete?")){
    var input = {courseid: cid};

    var postbackUrl = "../src/controller/coursecontroller.php/DeleteCourse";
    __DoAsyncPostBack(
      input,
      postbackUrl,
      Course.DeleteCourseCallBack,
      "error.html"
    );
}
  },

  DeleteCourseCallBack: function (AjaxOut){

	if(AjaxOut.Success){
		alert('Deleted!');
		window.location.reload();
	}else{
		
	}

  }

};
