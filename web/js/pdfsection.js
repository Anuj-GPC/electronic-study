PDFSection = {
    Init: function() {
      PDFSection.ListOfSecPDFDisplay();
    },
  
    RedirectToAddPDFPage: function() {
      window.location.href = "addpdfsection.html";
    },
  
    ListOfSecPDFDisplay: function() {
      var input = {};
  
      var postbackUrl = "../src/controller/pdfsectioncontroller.php/ListOfSecPDFDisplay";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        PDFSection.ListOfSecPDFDisplayCallBack,
        "error.html"
      );
    },
  
    ListOfSecPDFDisplayCallBack: function(AjaxOut) {
      if (AjaxOut.Success) {
        var i;
        var Count = AjaxOut.Result.RowCount;
        var BindHtml = "";
        var path;
        for (i = 0; i < Count; i++) {
           path='../../files/'+AjaxOut.Result.Data[i].PDFID+'/';
          path = path + AjaxOut.Result.Data[i].FileName
          BindHtml =
            BindHtml +
            "<tr>" +
            "<td>" +
            parseInt(i + 1) +
            "</td>" +
            "<td>" +
            AjaxOut.Result.Data[i].CName +
            "</td>" +
			"<td>" +
            AjaxOut.Result.Data[i].SName +
            "</td>" +
			"<td>" +
            AjaxOut.Result.Data[i].Name +
            "</td>" +
            "<td>" +
            AjaxOut.Result.Data[i].PDFsectionName +
            "</td>" +
            '<td class="text-primary">' +
            '<a href="addpdfsection.html?pid='+ AjaxOut.Result.Data[i].PSID +'"><i class="fa fa-edit"></i></a><a title="View" href="'+ path +'" target="_blank"><i class="fa fa-eye"></i></a>  <a href="#" onclick="PDFSection.DeletePDF('+ AjaxOut.Result.Data[i].PSID +')"><i class="fa fa-trash-o"></i></a>' +
            "</td>" +
            "</tr>";

        }
        $("#PDFDisplay").html(BindHtml);
      } else {
      }
    },
        
    DeletePDF: function(pid) {
        if(confirm ("Are you sure to delete?")){
      var input = {pdfid: pid};
  
      var postbackUrl = "../src/controller/pdfsectioncontroller.php/DeletePDF";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        PDFSection.DeletePDFCallBack,
        "error.html"
      );
        }
    },
  
    DeletePDFCallBack: function (AjaxOut){
  
      if(AjaxOut.Success){
          alert('Deleted!');
          window.location.reload();
      }else{
          
      }
  
    }
  
  };
  