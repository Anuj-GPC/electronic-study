User = {
    Init: function() {
      $( "#from" ).datepicker(
        { dateFormat: 'yy-mm-dd'}
      ).datepicker("setDate", -1);
      $( "#to" ).datepicker(
        { dateFormat: 'yy-mm-dd', }
      ).datepicker("setDate", new Date());
      User.ListOfUser();
    },
  
   
    ListOfUser: function() {
      var From =$('#from').val();
      var To =$('#to').val();
      
      var input = {from:From,to:To};
  
      var postbackUrl = "../src/controller/usercontroller.php/ListOfUser";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        User.ListOfUserCallBack,
        "error.html"
      );
    },
  
    ListOfUserCallBack: function(AjaxOut) {
      $("#UserDisplay").html('');
      if (AjaxOut.Success) {
        
        var i;
        var Count = AjaxOut.Result.RowCount;
        if(Count > 0){
        var BindHtml = "";
        var path;
        for (i = 0; i < Count; i++) {
        
          BindHtml =
            BindHtml +
            "<tr>" +
            "<td>" +
            parseInt(i + 1) +
            "</td>" +
            "<td>" +
            AjaxOut.Result.Data[i].Name +
            "</td>" +
			"<td>" +
            AjaxOut.Result.Data[i].Email +
            "</td>" +
			"<td>" +
            AjaxOut.Result.Data[i].Phone +
            "</td>" +
            
            '<td class="text-primary">' +
            '<a title="See List" href="#" onclick="User.ViewPDFBuyList('+  AjaxOut.Result.Data[i].ID +')"><i class="fa fa-eye"></i></a> ' +
            "</td>" +
            "</tr>";

        }
        $("#UserDisplay").html(BindHtml);
      }else{
        $("#UserDisplay").html('No User!');
      }
      } else {
      }
    },

    ViewPDFBuyList: function(id) {
    
      
      var input = {userid:id};
  
      var postbackUrl = "../src/controller/usercontroller.php/ViewPDFBuyList";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        User.ViewPDFBuyListCallBack,
        "error.html"
      );
    },
    ViewPDFBuyListCallBack: function(AjaxOut) {

      
      $("#UserBoughtList").html('');
      if (AjaxOut.Success) {
        var i;
        var Count = AjaxOut.Result.RowCount;
        if(Count > 0){
        var BindHtml = "";
        var path;
        for (i = 0; i < Count; i++) {
        
          BindHtml =
            BindHtml +
            "<tr>" +
            "<td>" +
            parseInt(i + 1) +
            "</td>" +
            "<td>" +
            AjaxOut.Result.Data[i].SName +
            "</td>" +
			"<td>" +
            AjaxOut.Result.Data[i].Name +
            "</td>" +
			"<td>" +
            AjaxOut.Result.Data[i].Price +
            "</td>" +
            "</tr>";

        }
        $("#UserBoughtList").html(BindHtml);
      }else{
        $("#UserBoughtList").html('No data');
      }
      jQuery.noConflict();      
      $('#userBuyPDFModal').modal('show');
      } else {
      }
    },
  
        
  
  
  };
  