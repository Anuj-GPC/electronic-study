AddPDF = {
    GlobStorePDFID: 0,
    Init: function() {
    
      AddPDF.ListOfCourse();

    var PDFID = __GetQueryStringParameterByName("pid");

    if (PDFID != "") {
      AddPDF.GlobStorePDFID = PDFID;
      AddPDF.GetParticularPDF(PDFID);
      $('.hideOnUpdatePDF').hide();
      $('#PdfTitle').html('Update PDF');
    }

   
    },
  
   
  
    ListOfCourse: function() {
        var input = {};
    
        var postbackUrl = "../src/controller/pdfcontroller.php/ListOfCourse";
        __DoAsyncPostBack(
          input,
          postbackUrl,
          AddPDF.ListOfCourseCallBack,
          "error.html"
        );
      },
    
      ListOfCourseCallBack: function(AjaxOut) {
        if (AjaxOut.Success) {
          var i;
          var Count = AjaxOut.Result.RowCount;
          var BindHtml = '<option value="Select">Select</option>';
          for (i = 0; i < Count; i++) {
            BindHtml =
              BindHtml +"<option value='"+ AjaxOut.Result.Data[i].ID +"'>"+ AjaxOut.Result.Data[i].Name +"</option>";
          }
          $("#SelectCourse").html(BindHtml);
        } else {
        }
      },

      GetCourseSubject : function (){
          var courseID = $('#SelectCourse').val();

            var input = {courseid:courseID};
    
        var postbackUrl = "../src/controller/pdfcontroller.php/GetCourseSubject";
        __DoAsyncPostBack(
          input,
          postbackUrl,
          AddPDF.GetCourseSubjectCallBack,
          "error.html"
        );
      },

       GetCourseSubjectCallBack: function(AjaxOut) {
        if (AjaxOut.Success) {
          var i;
          var Count = AjaxOut.Result.RowCount;
          var BindHtml = '<option value="Select">Select Course</option>';
          for (i = 0; i < Count; i++) {
            BindHtml =
              BindHtml +"<option value='"+ AjaxOut.Result.Data[i].ID +"'>"+ AjaxOut.Result.Data[i].Name +"</option>";
          }
          $("#SelectSubject").html(BindHtml);
        } else {
        }
      },

   
      GetParticularPDF: function(pid) {
        var input = { pdfid: pid };
    
        var postbackUrl =
          "../src/controller/pdfcontroller.php/GetParticularPDF";
        __DoAsyncPostBack(
          input,
          postbackUrl,
          AddPDF.GetParticularPDFCallBack,
          "error.html"
        );
      },
    
      GetParticularPDFCallBack: function(AjaxOut) {
        if (AjaxOut.Success) {
          $("#Name").val(AjaxOut.Result.Name);
          $("#PDFPrice").val(AjaxOut.Result.Price);
          $("#SelectFileType").val(AjaxOut.Result.FileType);
          if(AjaxOut.Result.FileType == 'Multiple File'){
            $('#pdfUploadElement').hide();
          }
          
        } else {
        }
      },
      
      handlePdfUploadElement:function (){
        var val=$('#SelectFileType').val();
        if(val == "Single File"){

          $('#pdfUploadElement').show();

        }else{
          $('#pdfUploadElement').hide();
        }
      }
    
  };

  // core js


  $("#btnSavePDF").on("click", function(e) {
    
  
    e.preventDefault();
  
    var formData = new FormData();
    var file = document.getElementById('PDFFile').files[0];
    var subjectID = $('#SelectSubject').val();
    var price = $('#PDFPrice').val();
	  var CourseID = $('#SelectCourse').val();
    var name = $('#Name').val();
    var fileType = $('#SelectFileType').val();
    if(fileType ==""){
      alert('please select file type');
      return;
    }
    
   
  
    formData.append("pdf", file);
    formData.append("type", 'pdfupload');
    formData.append("subjectid", subjectID);
    formData.append("price", price);
	  formData.append("courseid", CourseID);
    formData.append("name", name);
    formData.append("filetype", fileType);

    // if update
    if(AddPDF.GlobStorePDFID !=""){
      formData.append("type", 'updatepdf');  
      formData.append("pdfid", AddPDF.GlobStorePDFID);  
    }
    if(typeof file !="undefined"){
    $("#progBar").show();
	}
    $.ajax({
      url: "../../src/core/fileupload.php",
      type: "POST",
      data: formData,
      contentType: false,
      cache: false,
      processData: false,
	    xhr: function () {
          var myXhr = $.ajaxSettings.xhr();
          if (myXhr.upload) {
            myXhr.upload.addEventListener("progress", progress, false);
          }
          return myXhr;
        },
      success: function(data) {
         $("#progBar").hide();
       alert(data);
       location.reload();
      },
      error: function() {}
    });
	resetProgressBar();
  });
   // Proress bar
  function progress(e) {
    if (e.lengthComputable) {
      $("progress").attr({ value: e.loaded, max: e.total });
      var percentage = (e.loaded / e.total) * 100;
      $("#prog").html(percentage.toFixed(0) + "%");
    }
  }

  //Reset progress bar
  function resetProgressBar() {
    $("#prog").html("0%");
    $("progress").attr({ value: 0, max: 100 });
  }
  