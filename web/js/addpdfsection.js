AddPDFSection = {
    GlobStorePDFID: 0,
    Init: function() {
    
      AddPDFSection.ListOfPDF();

    var PDFID = __GetQueryStringParameterByName("pid");

    if (PDFID != "") {
      AddPDFSection.GlobStorePDFID = PDFID;
      $('.hideOnUpdatePDF').hide();
      $('#SelectPDF').attr('disabled','disabled');
      AddPDFSection.GetParticularPDF(PDFID);
      
      $('#PdfTitle').html('Update PDF');
    }

   
    },
  
   
  
    ListOfPDF: function() {
        var input = {};
    
        var postbackUrl = "../src/controller/pdfsectioncontroller.php/ListOfPDF";
        __DoAsyncPostBack(
          input,
          postbackUrl,
          AddPDFSection.ListOfPDFCallBack,
          "error.html"
        );
      },
    
      ListOfPDFCallBack: function(AjaxOut) {
        if (AjaxOut.Success) {
          var i;
          var Count = AjaxOut.Result.RowCount;
          var BindHtml = '<option value="">Select PDF (Course -> Subject)</option>';
          for (i = 0; i < Count; i++) {
            BindHtml =
              BindHtml +"<option value='"+ AjaxOut.Result.Data[i].PID +"'>"+ AjaxOut.Result.Data[i].Name +"("+ AjaxOut.Result.Data[i].CName +" -> "+AjaxOut.Result.Data[i].SName +")</option>";
          }
          $("#SelectPDF").html(BindHtml);
        } else {
        }
      },

      GetMultiFilePdf : function (){
          var PDFID = $('#SelectPDF').val();

            var input = {pdfid:PDFID};
    
        var postbackUrl = "../src/controller/pdfsectioncontroller.php/GetMultiFilePdf";
        __DoAsyncPostBack(
          input,
          postbackUrl,
          AddPDFSection.GetMultiFilePdfCallBack,
          "error.html"
        );
      },

      GetMultiFilePdfCallBack: function(AjaxOut) {
        if (AjaxOut.Success) {
          $('#SelectCourse').val(AjaxOut.Result.CName);
          $('#SelectSubject').val(AjaxOut.Result.SName);

         
        } else {
        }
      },

   
      GetParticularPDF: function(pid) {
        var input = { pdfid: pid };
    
        var postbackUrl =
          "../src/controller/pdfsectioncontroller.php/GetParticularPDF";
        __DoAsyncPostBack(
          input,
          postbackUrl,
          AddPDFSection.GetParticularPDFCallBack,
          "error.html"
        );
      },
    
      GetParticularPDFCallBack: function(AjaxOut) {
        if (AjaxOut.Success) {
          $("#Name").val(AjaxOut.Result.Name);
          $("#SelectPDF").val(AjaxOut.Result.PDFID);
          
        } else {
        }
      },
    
  };

  // core js


  $("#btnSavePDF").on("click", function(e) {
    
  
    e.preventDefault();
  
    var formData = new FormData();
    var file = document.getElementById('PDFFile').files[0];
    var PdfSecID = $('#SelectPDF').val();
    var name = $('#Name').val();
    
   
  
    formData.append("pdf", file);
    formData.append("type", 'pdfsectionupload');
	  formData.append("pdfsecid", PdfSecID);
    formData.append("name", name);

    // if update
    if(AddPDFSection.GlobStorePDFID !=""){
      formData.append("type", 'updatesectionpdf');  
      formData.append("pdfid", AddPDFSection.GlobStorePDFID);  
    }
    if(typeof file !="undefined"){
    $("#progBar").show();
	}
    $.ajax({
      url: "../../src/core/fileupload.php",
      type: "POST",
      data: formData,
      contentType: false,
      cache: false,
      processData: false,
	    xhr: function () {
          var myXhr = $.ajaxSettings.xhr();
          if (myXhr.upload) {
            myXhr.upload.addEventListener("progress", progress, false);
          }
          return myXhr;
        },
      success: function(data) {
         $("#progBar").hide();
       alert(data);
       location.reload();
      },
      error: function() {}
    });
	resetProgressBar();
  });
   // Proress bar
  function progress(e) {
    if (e.lengthComputable) {
      $("progress").attr({ value: e.loaded, max: e.total });
      var percentage = (e.loaded / e.total) * 100;
      $("#prog").html(percentage.toFixed(0) + "%");
    }
  }

  //Reset progress bar
  function resetProgressBar() {
    $("#prog").html("0%");
    $("progress").attr({ value: 0, max: 100 });
  }
  