Subject = {
    Init: function() {
      Subject.ListOfSubject();
    },
  
    RedirectToAddSubjectPage: function() {
      window.location.href = "addsubject.html";
    },
  
    ListOfSubject: function() {
      var input = {};
  
      var postbackUrl = "../src/controller/subjectcontroller.php/ListOfSubject";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        Subject.ListOfSubjectCallBack,
        "error.html"
      );
    },
  
    ListOfSubjectCallBack: function(AjaxOut) {
      if (AjaxOut.Success) {
        var i;
        var Count = AjaxOut.Result.RowCount;
        var BindHtml = "";
        for (i = 0; i < Count; i++) {
          BindHtml =
            BindHtml +
            "<tr>" +
            "<td>" +
            parseInt(i + 1) +
            "</td>" +
            "<td>" +
            AjaxOut.Result.Data[i].CName +
            "</td>" +
            "<td>" +
            AjaxOut.Result.Data[i].SName +
            "</td>" +
            '<td class="text-primary">' +
            '<a href="addsubject.html?sid='+ AjaxOut.Result.Data[i].ID +'"><i class="fa fa-edit"></i></a>  <a href="#" onclick="Subject.DeleteSubject('+ AjaxOut.Result.Data[i].ID +')"><i class="fa fa-trash-o"></i></a>' +
            "</td>" +
            "</tr>";
        }
        $("#SubjectDisplay").html(BindHtml);
      } else {
      }
    },
        
    DeleteSubject: function(sid) {
        if(confirm ("Are you sure to delete?")){
      var input = {subjectid: sid};
  
      var postbackUrl = "../src/controller/subjectcontroller.php/DeleteSubject";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        Subject.DeleteSubjectCallBack,
        "error.html"
      );
        }
    },
  
    DeleteSubjectCallBack: function (AjaxOut){
  
      if(AjaxOut.Success){
          alert('Deleted!');
          window.location.reload();
      }else{
          
      }
  
    }
  
  };
  