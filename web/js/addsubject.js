AddSubject = {
    GlobStoreSubjectID: 0,
    Init: function() {
      var SubjectID = __GetQueryStringParameterByName("sid");
      AddSubject.ListOfCourse();

      if (SubjectID != "") {
        AddSubject.GlobStoreSubjectID = SubjectID;
        AddSubject.GetParticularSubject(SubjectID);
        $("#btnSaveSubject").attr("onclick", "AddSubject.UpdateSubject();");
      }
    },
  
    AddSubject: function() {
      var SubjectName = $("#SubjectName").val();
      var CourseID = $("#SelectCourse").val();
  
      var input = { subjectname: SubjectName ,courseid:CourseID};
  
      var postbackUrl = "../src/controller/subjectcontroller.php/AddSubject";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        AddSubject.AddSubjectCallBack,
        "error.html"
      );
    },
  
    AddSubjectCallBack: function(AjaxOut) {
      if (AjaxOut.Success) {
        $("#SuccessMessage").fadeIn();
        $("#SuccessMessage").fadeOut(1000);
        document.getElementById("AddSubjectForm").reset();
      } else {
        $("#ErrorMessage").fadeIn();
        $("#ErrorMessage").fadeOut(1000);
      }
    },
  
    GetParticularSubject: function(sid) {
      var input = { subjectid: sid };
  
      var postbackUrl =
        "../src/controller/subjectcontroller.php/GetParticularSubject";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        AddSubject.GetParticularSubjectCallBack,
        "error.html"
      );
    },
  
    GetParticularSubjectCallBack: function(AjaxOut) {
      if (AjaxOut.Success) {
        $("#SelectCourse").val(AjaxOut.Result.CourseID);
        $("#SubjectName").val(AjaxOut.Result.Name);
      } else {
      }
    },
  
    UpdateSubject: function() {
        var SubjectName = $("#SubjectName").val();
        var CourseID = $("#SelectCourse").val();
    
      var input = { subjectid: AddSubject.GlobStoreSubjectID,subjectname :SubjectName,courseid:CourseID };
  
      var postbackUrl = "../src/controller/subjectcontroller.php/UpdateSubject";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        AddSubject.UpdateSubjectCallBack,
        "error.html"
      );
    },
  
    UpdateSubjectCallBack: function(AjaxOut) {
      if (AjaxOut.Success) {
        $("#SuccessMessage").fadeIn();
        $("#SuccessMessage").fadeOut(1000);
        
      } else {
        $("#ErrorMessage").fadeIn();
        $("#ErrorMessage").fadeOut(1000);
      }
    },

    ListOfCourse: function() {
        var input = {};
    
        var postbackUrl = "../src/controller/subjectcontroller.php/ListOfCourse";
        __DoAsyncPostBack(
          input,
          postbackUrl,
          AddSubject.ListOfCourseCallBack,
          "error.html"
        );
      },
    
      ListOfCourseCallBack: function(AjaxOut) {
        if (AjaxOut.Success) {
          var i;
          var Count = AjaxOut.Result.RowCount;
          var BindHtml = '<option value="Select">Select</option>';
          for (i = 0; i < Count; i++) {
            BindHtml =
              BindHtml +
              "<option value='"+AjaxOut.Result.Data[i].ID +"'>" +
              AjaxOut.Result.Data[i].Name +
              "</option>";
          }
          $("#SelectCourse").html(BindHtml);
        } else {
        }
      },
  };
  