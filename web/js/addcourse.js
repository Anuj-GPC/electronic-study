AddCourse = {
  GlobStoreCourseID: 0,
  Init: function() {
    var CourseID = __GetQueryStringParameterByName("cid");

    if (CourseID != "") {
      AddCourse.GlobStoreCourseID = CourseID;
      AddCourse.GetParticularCourse(CourseID);
      $("#btnSaveCourse").attr("onclick", "AddCourse.UpdateCourse();");
    }
  },

  AddCourse: function() {
    var CourseName = $("#CourseName").val();

    var input = { coursename: CourseName };

    var postbackUrl = "../src/controller/addcoursecontroller.php/AddCourse";
    __DoAsyncPostBack(
      input,
      postbackUrl,
      AddCourse.AddCourseCallBack,
      "error.html"
    );
  },

  AddCourseCallBack: function(AjaxOut) {
    if (AjaxOut.Success) {
      $("#SuccessMessage").fadeIn();
      $("#SuccessMessage").fadeOut(1000);
      document.getElementById("AddCourseForm").reset();
    } else {
      $("#ErrorMessage").fadeIn();
      $("#ErrorMessage").fadeOut(1000);
    }
  },

  GetParticularCourse: function(cid) {
    var input = { courseid: cid };

    var postbackUrl =
      "../src/controller/addcoursecontroller.php/GetParticularCourse";
    __DoAsyncPostBack(
      input,
      postbackUrl,
      AddCourse.GetParticularCourseCallBack,
      "error.html"
    );
  },

  GetParticularCourseCallBack: function(AjaxOut) {
    if (AjaxOut.Success) {
      $("#CourseName").val(AjaxOut.Result.Name);
    } else {
    }
  },

  UpdateCourse: function() {
    var CourseName = $("#CourseName").val();
    var input = { courseid: AddCourse.GlobStoreCourseID,coursename :CourseName };

    var postbackUrl = "../src/controller/addcoursecontroller.php/UpdateCourse";
    __DoAsyncPostBack(
      input,
      postbackUrl,
      AddCourse.UpdateCourseCallBack,
      "error.html"
    );
  },

  UpdateCourseCallBack: function(AjaxOut) {
    if (AjaxOut.Success) {
      $("#SuccessMessage").fadeIn();
      $("#SuccessMessage").fadeOut(1000);
      
    } else {
      $("#ErrorMessage").fadeIn();
      $("#ErrorMessage").fadeOut(1000);
    }
  }
};
