PDF = {
    Init: function() {
      PDF.ListOfPDF();
    },
  
    RedirectToAddPDFPage: function() {
      window.location.href = "addpdf.html";
    },
  
    ListOfPDF: function() {
      var input = {};
  
      var postbackUrl = "../src/controller/pdfcontroller.php/ListOfPDF";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        PDF.ListOfPDFCallBack,
        "error.html"
      );
    },
  
    ListOfPDFCallBack: function(AjaxOut) {
      if (AjaxOut.Success) {
        var i;
        var Count = AjaxOut.Result.RowCount;
        var BindHtml = "";
        var path;
        var FileType;
        for (i = 0; i < Count; i++) {
          if(AjaxOut.Result.Data[i].FileType == null){
              FileType= '-';
          }else{
            FileType =  AjaxOut.Result.Data[i].FileType;
          }
         
           path='../../files/';
          path = path + AjaxOut.Result.Data[i].FileName
          BindHtml =
            BindHtml +
            "<tr>" +
            "<td>" +
            parseInt(i + 1) +
            "</td>" +
            "<td>" +
            AjaxOut.Result.Data[i].CName +
            "</td>" +
			"<td>" +
            AjaxOut.Result.Data[i].SName +
            "</td>" +
			"<td>" +
            AjaxOut.Result.Data[i].Name +
            "</td>" +
            "<td>" +
            AjaxOut.Result.Data[i].Price +
            "</td>" +
            "<td>" +
            FileType +
            "</td>" +
            '<td class="text-primary">' +
            '<a href="addpdf.html?pid='+ AjaxOut.Result.Data[i].PID +'"><i class="fa fa-edit"></i></a><a id="ViewPdf'+i+'" title="View" href="'+ path +'" target="_blank"><i class="fa fa-eye"></i></a>  <a href="#" onclick="PDF.DeletePDF('+ AjaxOut.Result.Data[i].PID +')"><i class="fa fa-trash-o"></i></a>' +
            "</td>" +
            "</tr>";

        }
        $("#PDFDisplay").html(BindHtml);
        var i;
        var FileType;
        for(i=0;i< Count;i++){
         FileType= AjaxOut.Result.Data[i].FileType
          if(FileType == 'Multiple File'){
            $('#ViewPdf'+i).addClass('hideViewBtn');
          }
          
        }
      } else {
      }
    },
        
    DeletePDF: function(pid) {
        if(confirm ("Are you sure to delete?")){
      var input = {pdfid: pid};
  
      var postbackUrl = "../src/controller/pdfcontroller.php/DeletePDF";
      __DoAsyncPostBack(
        input,
        postbackUrl,
        PDF.DeletePDFCallBack,
        "error.html"
      );
        }
    },
  
    DeletePDFCallBack: function (AjaxOut){
  
      if(AjaxOut.Success){
          alert('Deleted!');
          window.location.reload();
      }else{
          
      }
  
    }
  
  };
  