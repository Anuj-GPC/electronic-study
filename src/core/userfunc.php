<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/dal/connection.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/ajaxout.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/usermodel.php';

class UserCall
{

   

   
      public function ListOfUser($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $User = new UserModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        $from = mysqli_real_escape_string($link, $data->from);
        $to = mysqli_real_escape_string($link, $data->to);
        
        
        $sql = "select * from users where Name <> 'Admin' ";

        if($from !='' && $to !=''){
            $sql.= "and DATE(DateCreated) between '$from' and '$to'";
        }
        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $User->Data[] = $val;

        }
        $User->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $User;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

   
    public function ViewPDFBuyList ($data){
        
        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $User = new UserModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        $userID = mysqli_real_escape_string($link, $data->userid);
        
        
        $sql = "select p1.Name,p1.Price,s.Name as SName 
        from payment p 
        inner join pdf p1 
        on p.PDFID = p1.ID 
        inner join subject s 
        on p1.SubjectID = s.ID  where p.userID=$userID";
        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $User->Data[] = $val;

        }
        $User->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $User;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;

    }

  }
