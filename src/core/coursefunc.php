<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/dal/connection.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/ajaxout.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/coursemodel.php';

class CourseCall
{

    public function ListOfCourse($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $Course = new CourseModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        
        
        $sql = "select * from course";
        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $Course->Data[] = $val;

        }
        $Course->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $Course;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function DeleteCourse($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        $courseid= mysqli_real_escape_string($link, $data->courseid);       
        $sql = "delete from course where ID =$courseid ";
        $sqlrun = mysqli_query($link,$sql);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

  }
