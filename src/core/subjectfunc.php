<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/dal/connection.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/ajaxout.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/subjectmodel.php';

class SubjectCall
{

    public function AddSubject($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $courseid = mysqli_real_escape_string($link, $data->courseid);
        $subjectname = mysqli_real_escape_string($link, $data->subjectname);
        
        $sql = "insert into subject set Name = '$subjectname',CourseID=$courseid";
        $sqlrun = mysqli_query($link,$sql);
        

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function GetParticularSubject($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $subjectid = mysqli_real_escape_string($link, $data->subjectid);
        
        $sql = "select * from subject where ID = $subjectid";
        $sqlrun = mysqli_query($link,$sql);
        $val = mysqli_fetch_array($sqlrun);
        

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $val;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function UpdateSubject($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $courseid= mysqli_real_escape_string($link, $data->courseid);
        $subjectid= mysqli_real_escape_string($link, $data->subjectid);
        $subjectname= mysqli_real_escape_string($link, $data->subjectname);
        
        $sql = "update subject set Name = '$subjectname',CourseID = '$courseid' where ID = $subjectid";
        $sqlrun = mysqli_query($link,$sql);
        

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

      public function ListOfCourse($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $Course = new SubjectModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        
        
        $sql = "select * from course";
        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $Course->Data[] = $val;

        }
        $Course->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $Course;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function ListOfSubject($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $Subject = new SubjectModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        
        
        $sql = "select s.ID,s.Name as SName,c.Name as CName from subject s inner join course c on s.CourseID=c.ID";
        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $Subject->Data[] = $val;

        }
        $Subject->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $Subject;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function DeleteSubject($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        $subjectid= mysqli_real_escape_string($link, $data->subjectid);       
        $sql = "delete from subject where ID =$subjectid ";
        $sqlrun = mysqli_query($link,$sql);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

  }
