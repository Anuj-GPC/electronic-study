<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/dal/connection.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/ajaxout.php';


class AddCourseCall
{

    public function AddCourse($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $coursename = mysqli_real_escape_string($link, $data->coursename);
        
        $sql = "insert into course set Name = '$coursename'";
        $sqlrun = mysqli_query($link,$sql);
        

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function GetParticularCourse($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $courseid = mysqli_real_escape_string($link, $data->courseid);
        
        $sql = "select * from course where ID = $courseid";
        $sqlrun = mysqli_query($link,$sql);
        $val = mysqli_fetch_array($sqlrun);
        

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $val;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function UpdateCourse($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $courseid= mysqli_real_escape_string($link, $data->courseid);
        $coursename= mysqli_real_escape_string($link, $data->coursename);
        
        $sql = "update course set Name = '$coursename' where ID = $courseid";
        $sqlrun = mysqli_query($link,$sql);
        

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

  }
