<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/dal/connection.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/ajaxout.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/pdfmodel.php';

class PDFSectionCall
{

   

   
      public function ListOfPDF($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $PDF = new PDFModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        
        
        $sql = "select p.ID as PID,p.Name,s.Name as SName,c.Name as CName from pdf p 
        inner join subject s on p.SubjectID =  s.ID inner join course
         c on c.ID = p.CourseID where p.FileType='Multiple File'";
        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $PDF->Data[] = $val;

        }
        $PDF->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $PDF;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function GetMultiFilePdf($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        $pdfID= mysqli_real_escape_string($link, $data->pdfid);       
        
        
        $sql = "select p.ID as PID,p.Name,s.Name as SName,c.Name as CName
         from pdf p inner join subject s on p.SubjectID =  s.ID inner join 
         course c on c.ID = p.CourseID where  p.ID=$pdfID";
        $sqlrun = mysqli_query($link,$sql);

        $val=mysqli_fetch_array($sqlrun);
      

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $val;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function DeletePDF($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $PDFid= mysqli_real_escape_string($link, $data->pdfid);       

        //unlink
        $sql = "select FileName,PDFID from pdfsection where ID =$PDFid ";
        $sqlrun = mysqli_query($link,$sql);
        $val = mysqli_fetch_array($sqlrun);
        $file = '../../files/'.$val['PDFID'].'/'.$val['FileName'];
        unlink($file);

        $sql = "delete from pdfsection where ID =$PDFid ";
        $sqlrun = mysqli_query($link,$sql);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function ListOfSecPDFDisplay ($data){
        
        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $PDF = new PDFModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        
        
        $sql = "select ps.ID as PSID, ps.PDFID,ps.Name as PDFsectionName,
        ps.FileName,p.Name,s.Name as SName,c.Name as CName
        from pdf p inner join subject s 
        on p.SubjectID =  s.ID inner join course c
        on c.ID = p.CourseID inner join pdfsection ps
        on ps.PDFID = p.ID";

        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $PDF->Data[] = $val;

        }
        $PDF->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $PDF;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;

    }

    public function GetParticularPDF($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $PDFid = mysqli_real_escape_string($link, $data->pdfid);
        
        $sql = "select * from pdfsection where ID = $PDFid";
        $sqlrun = mysqli_query($link,$sql);
        $val = mysqli_fetch_array($sqlrun);
        

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $val;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }


  }
