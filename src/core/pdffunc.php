<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/dal/connection.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/ajaxout.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/model/pdfmodel.php';

class PDFCall
{

   

   
      public function ListOfPDF($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $PDF = new PDFModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        
        
        $sql = "select p.ID as PID,p.FileName,p.Name,s.Name as SName,p.Price,c.Name as CName,p.FileType
         from pdf p 
         inner join subject s on p.SubjectID =  s.ID
          inner join course c on c.ID = p.CourseID";
        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $PDF->Data[] = $val;

        }
        $PDF->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $PDF;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function GetCourseSubject($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $PDF = new PDFModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        $courseID= mysqli_real_escape_string($link, $data->courseid);       
        
        
        $sql = "select * from subject where CourseID =$courseID";
        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $PDF->Data[] = $val;

        }
        $PDF->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $PDF;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function DeletePDF($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $PDFid= mysqli_real_escape_string($link, $data->pdfid);       

        //unlink
        $sql = "select FileName from pdf where ID =$PDFid ";
        $sqlrun = mysqli_query($link,$sql);
        $val = mysqli_fetch_array($sqlrun);
        $file = '../../files/'.$val['FileName'];
        unlink($file);

        $sql = "delete from pdf where ID =$PDFid ";
        $sqlrun = mysqli_query($link,$sql);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }

    public function ListOfCourse ($data){
        
        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        $PDF = new PDFModel();
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        
        
        $sql = "select * from course";
        $sqlrun = mysqli_query($link,$sql);

        while($val=mysqli_fetch_array($sqlrun)){

            $PDF->Data[] = $val;

        }
        $PDF->RowCount = mysqli_num_rows($sqlrun);

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $PDF;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;

    }

    public function GetParticularPDF($data)
    {


        $AjaxOuts = new AjaxOut();
        $link = StartConnection();
        
        session_start();

        if (!$link) {
            $AjaxOuts->Success = false;
            $AjaxOuts->Message = "Cannot Connect to the network.";
            return $AjaxOuts;

        }
        
        $PDFid = mysqli_real_escape_string($link, $data->pdfid);
        
        $sql = "select * from pdf where ID = $PDFid";
        $sqlrun = mysqli_query($link,$sql);
        $val = mysqli_fetch_array($sqlrun);
        

        if ($sqlrun) {

            $AjaxOuts->Success = true;
            $AjaxOuts->Result = $val;
            $AjaxOuts->Message = 'Success';
            return $AjaxOuts;
        }

        $AjaxOuts->Message = "Error!";
        $AjaxOuts->Success = false;

        return $AjaxOuts;
    }


  }
