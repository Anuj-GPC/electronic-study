<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/dal/connection.php';


//$link = StartConnection();

$Type = $_POST['type'];


$PDFUpload = new PDFUploadClass();

switch ($Type) {
        case 'pdfupload':

        $PDFUpload->PDFFileUpload();

        break;
        case 'updatepdf':

        $PDFUpload->PDFUpdate();
        
        break;
        case 'pdfsectionupload':

        $PDFUpload->PDFSectionFileUpload();

        break;
        case 'updatesectionpdf':

        $PDFUpload->PDFUpdateSection();

        break;

}

class PDFUploadClass
{

    public function PDFFileUpload()
    {
		

        $link = StartConnection();
        session_start();
       

        $valid_formats = array("pdf");

        $tmp = $_FILES['pdf']['tmp_name'];
        $pdf = $_FILES['pdf']['name'];
        $subjectID = $_POST['subjectid'];
        $price = $_POST['price'];
		$courseID = $_POST['courseid'];
        $name = $_POST['name'];
        $fileType = $_POST['filetype'];

        if($fileType=="Multiple File"){
            $sql = "insert into pdf set FileType='$fileType',CourseID=$courseID,Name='$name', SubjectID = $subjectID, Price='$price'";
            $sqlrun = mysqli_query($link, $sql);
            if ($sqlrun) {
                echo 'Saved!';
            } else {
                echo 'Error!, Try again.';
            }
			die;


        }
		
        $file_ext = strtolower(end(explode('.', $pdf)));
        if (in_array($file_ext, $valid_formats) === false) {
            echo "Wrong Format!";
            exit;
        }
       
        $pdfname = time() . rand(0, 120192830) . ".pdf";


        if (move_uploaded_file($tmp, "../../files/$pdfname")) {
			
        
            $sql = "insert into pdf set FileType='$fileType',CourseID=$courseID,Name='$name', SubjectID = $subjectID, FileName='$pdfname',Price='$price'";
            $sqlrun = mysqli_query($link, $sql);
            if ($sqlrun) {
                echo 'Uploaded!';
            } else {
                echo 'Error!, Try again.';
            }

        }
        die;
    }

    public function PDFUpdate (){

        $link = StartConnection();
        session_start();
       

        $valid_formats = array("pdf");

        $tmp = $_FILES['pdf']['tmp_name'];
        $pdf = $_FILES['pdf']['name'];
        $price = $_POST['price'];
        $name = $_POST['name'];
        $pdfid = $_POST['pdfid'];
        $fileType = $_POST['filetype'];
        
		
		if($pdf ==''){
		 $sql = "update pdf set FileType='$fileType',Name='$name',Price='$price' where ID=$pdfid";
            $sqlrun = mysqli_query($link, $sql);
            if ($sqlrun) {
                echo 'Updated!';
            } else {
                echo 'Error!, Try again.';
            }
			die;
		}
			
		
        $file_ext = strtolower(end(explode('.', $pdf)));
        if (in_array($file_ext, $valid_formats) === false) {
            echo "Wrong Format!";
            exit;
        }
       
        $pdfname = time() . rand(0, 120192830) . ".pdf";


        if (move_uploaded_file($tmp, "../../files/$pdfname")) {
			
       
        
            $sql = "update pdf set FileType='$fileType',Name='$name',Price='$price',FileName='$pdfname' where ID=$pdfid";
            $sqlrun = mysqli_query($link, $sql);
            if ($sqlrun) {
                echo 'Updated!';
            } else {
                echo 'Error!, Try again.';
            }
		}
        
        die;

    }

    public function PDFSectionFileUpload(){


        $link = StartConnection();
        session_start();
       

        $valid_formats = array("pdf");

        $tmp = $_FILES['pdf']['tmp_name'];
        $pdf = $_FILES['pdf']['name'];
        $PdfSecID = $_POST['pdfsecid'];
        $name = $_POST['name'];
        		
        $file_ext = strtolower(end(explode('.', $pdf)));
        if (in_array($file_ext, $valid_formats) === false) {
            echo "Wrong Format!";
            exit;
        }
        if (!file_exists('../../files/'.$PdfSecID.'/')) {
            mkdir('../../files/'.$PdfSecID.'/', 0777, true);
        }
       
        $pdfname = time() . rand(0, 120192830) . ".pdf";


        if (move_uploaded_file($tmp, "../../files/$PdfSecID/$pdfname")) {
			
        
            $sql = "insert into pdfsection set Name='$name', FileName='$pdfname',PDFID='$PdfSecID'";
            $sqlrun = mysqli_query($link, $sql);
            if ($sqlrun) {
                echo 'Uploaded!';
            } else {
                echo 'Error!, Try again.';
            }

        }
        die;


    }
    public function PDFUpdateSection(){

        $link = StartConnection();
        session_start();
       

        $valid_formats = array("pdf");

        $tmp = $_FILES['pdf']['tmp_name'];
        $pdf = $_FILES['pdf']['name'];
        $name = $_POST['name'];
        $pdfid = $_POST['pdfid'];
        $PdfSecID =$_POST['pdfsecid'];
        
        
		
		if($pdf ==''){
		 $sql = "update pdfsection set Name='$name' where ID=$pdfid";
            $sqlrun = mysqli_query($link, $sql);
            if ($sqlrun) {
                echo 'Updated!';
            } else {
                echo 'Error!, Try again.';
            }
			die;
		}
			
		
        $file_ext = strtolower(end(explode('.', $pdf)));
        if (in_array($file_ext, $valid_formats) === false) {
            echo "Wrong Format!";
            exit;
        }
       
        $pdfname = time() . rand(0, 120192830) . ".pdf";


        if (move_uploaded_file($tmp, "../../files/$PdfSecID/$pdfname")) {
			
       
        
            $sql = "update pdfsection set Name='$name',FileName='$pdfname' where ID=$pdfid";
            $sqlrun = mysqli_query($link, $sql);
            if ($sqlrun) {
                echo 'Updated!';
            } else {
                echo 'Error!, Try again.';
            }
		}
        
        die;


    }
    

}


?>
