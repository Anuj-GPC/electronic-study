<?php

class AppConfig
{
    public function dirnname()
    {
        $basepath = dirname(dirname($_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['PHP_SELF']))) . '/';
        return $basepath;
    }

    public function BaseURL(){
        return "https://".$_SERVER['SERVER_NAME'];
    }

    public function TTSURL(){
        return "https://devprovisitytts.imcstech.com/web/html/Mp3asp.asp?";
    }

    public function Enviornment(){
        return "dev";
    }

    public function Platform() {
        // 'linux' or 'windows'
        return 'windows';
    }

    public function PDFConverter() {
        $config = new AppConfig;
        $platform = $config->Platform();
        

        if ($platform == 'linux') {
            $output = '/usr/local/bin/wkhtmltopdf';
        }

        if ($platform == 'windows') {
            $Dir = $_SERVER['DOCUMENT_ROOT'];
            $output = $Dir. '/extcomp/wkhtmltopdf/bin/wkhtmltopdf.exe';
        }

        return $output;
    }

    public function ImageConverter() {
        $config = new AppConfig;
        $platform = $config->Platform();

        if ($platform == 'linux') {
            $output = '/usr/local/bin/wkhtmltoimage';
        }

        if ($platform == 'windows') {
            $Dir = $_SERVER['DOCUMENT_ROOT'];
            $output = $Dir. '/extcomp/wkhtmltopdf/bin/wkhtmltoimage.exe';
        }

        return $output;
    }

    public function LameEncoder() {
        $config = new AppConfig;
        $platform = $config->Platform();

        if ($platform == 'linux') {
            $output = '/usr/bin/lame -f -V 9';
        }

        if ($platform == 'windows') {
            $Dir = $_SERVER['DOCUMENT_ROOT'];
            $output = $Dir. '/extcomp/mp3Genrater/TTS/lame.exe -V2 --vbr-new';
        }

        return $output;
    }

    public function mmfuncRoboCopy($src, $dest) {
        $config = new AppConfig;
        $platform = $config->Platform();

        if ($platform == 'linux') {
            exec("cp -r $src/* $dest");
        }

        if ($platform == 'windows') {
            exec("C:\Windows\System32\Robocopy.exe $src $dest /x /e");
        }

        return $output;
    }

    public function RemoveDir() {
        $config = new AppConfig;
        $platform = $config->Platform();

        if ($platform == 'linux') {
            $output = '/bin/rm -r -f';
        }

        if ($platform == 'windows') {
            $output = 'rd /s /q';
        }

        return $output;
    }

    public function FileUploadSize($size){

        // It is limited for 10 MB 
        if ($size > 20971520 || $size == 0) {
            return false;
        }

        return true;

    }

   

}

class StripeKey{

    // test
    const SECRET_KEY = 'sk_test_M47Np1NnBwc6Y3xaQruJ3EbA';
    const PUBLIC_KEY = 'pk_test_3Pon3sO8NkJjR5tTaU7wUYwj';

    // live
    // const SECRET_KEY = 'sk_live_FuC2UVcYXMIbfigb19mtqhII';
    // const PUBLIC_KEY = 'pk_live_aNexdQOtBr2MIjSazuKrePR8';

    

}
