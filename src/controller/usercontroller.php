<?php

//require_once ('../dal/connection.php');

//----------------------Post Data Collection ---------------------------------------------------------------------
$Post_FUNCTION = $_POST["POST_FUNCTION"]; //function name that needs executed
$POST_JSONDATA = $_POST["POST_JSONDATA"]; // this raw string data
$data = json_decode($POST_JSONDATA); // data converted to PHP object
$Out_JSONDATA;
// $AjaxOut; // property, AjaxOut.Success, AjaxOut.Result, AjaxOut.Message, AjaxOut.Sender, AjaxOut.Argumnets
//--------------------------------------------------------------------------------------------------------------------
require_once '../core/userfunc.php';
$call_Func = new UserCall(); //class call

//echo $inpPostData; //this returns json output assumes the $inpPostData is json string  -- tested -- we will not need this type of implementation
//--------------------------------------------------------------------------------------------------------------------
//echo json_encode($Post_FUNCTION); // this returns json data after converting the php object to json string
//------------------------------------------------------------------------------------------------------------------

//--------------------------------------function controller-------------------------------------------------------------------------------------

switch ($Post_FUNCTION) {

    case "ListOfUser":
        $Out_JSONDATA = $call_Func->ListOfUser($data);
        break;
    case 'ViewPDFBuyList':
        $Out_JSONDATA = $call_Func->ViewPDFBuyList($data);
        break;
   
   
    

    default:
        break;
}
//-------------------------------------------- The output -----------------------------------------------------------------------------------
echo json_encode($Out_JSONDATA); // this type of implementation is better thatn individual function return to avoid error of multiple returns
//-------------------------------------------------------------------------------------------------------------------------------------------

//======================================================== Function implementations ======================================================
