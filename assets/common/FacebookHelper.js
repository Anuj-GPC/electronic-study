FacebookHelper = {

    init() {
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        window.fbAsyncInit = function() {
            var appId = '264443320829718';
            FB.init({
                appId: appId,
                xfbml: true,
                version: 'v2.9'
            });
        };

    },

    ShareWithParameters: function(title, description, url, image) {

        //initialize facebook instance and download liberaries
        FacebookHelper.init();

        // Dynamically gather and set the FB share data. 
        var FBTitle = title;
        var FBDesc = description;
        var FBLink = url;
        var FBPic = image;

        // Open FB share popup
        FB.ui({
                method: 'share_open_graph',
                action_type: 'og.shares',
                action_properties: JSON.stringify({
                    object: {
                        'og:url': FBLink,
                        'og:title': FBTitle,
                        'og:description': FBDesc,
                        'og:image': FBPic
                    }
                })
            },
            function(response) {
                // Action after response
                //console.log(response);
            })
    }
}