var ImagestoDisplay = '';
var ImagesTimetoDisplay = 0;
var ImageSrc = '';
var VideoImagesHeight=0;

function AssignVideoSrc(id) {
    document.getElementById('videosrc').innerHTML = "";
    var input = { vid: id };
    var postbackUrl = '../../src/controller/listingvideocontroller.php/AssignVideoSrc';
    __DoAsyncPostBack(input, postbackUrl, AssignVideoSrcCallBack, 'error.html');
}

function AssignVideoSrcCallBack(AjaxOut) {
    if (AjaxOut.Success) {
      
        $('#videoaudio').attr('src', AjaxOut.Result.BackgroudMusic);
        $("#listvideo")[0].load();
    }
}

function SliderImage(id) {

    var input = { id: id };

    var postbackUrl = '../../src/controller/listingvideocontroller.php/SliderImage';
    __DoAsyncPostBack(input, postbackUrl, SliderImageCallBack, 'error.html');
}

function SliderImageCallBack(AjaxOut) {
    if (AjaxOut.Success) {
       // var imageSrc;
        var imgPrevContainer = document.getElementById('viewlistingvideoContainer');
       
        //draw a box maintainig the aspect ratio 300 X 400 -- Agent profile 3: 4
        //responsince designs widths are managed , heights need to be calculated
        var imgConWidth = imgPrevContainer.clientWidth;
        var imgContHeight = imgConWidth * (3 / 4);
        imgPrevContainer.style.height = imgContHeight + 'px';
        VideoImagesHeight = imgContHeight;


        $('#mainssimage').css({ "object-fit": "contain" });
       
        $('#wowslider-container1').css({ "object-fit": "contain" });

        if (AjaxOut.Result.Image[0].Type == "listing") {
            var src = '"../../media/listings/' + AjaxOut.Result.Image[0].ListImageID + '/photo/' + AjaxOut.Result.Image[0].ImageName + '"';
            url = self.origin + '/media/listings/' + AjaxOut.Result.Image[0].ListImageID + '/photo/' + AjaxOut.Result.Image[0].ImageName;
        } else if (AjaxOut.Result.Image[0].Type == "listingslide") {
            src = '../../media/listings/' + AjaxOut.Result.Image[0].ListImageID + '/videoslides/' + AjaxOut.Result.Image[0].ImageName;
            url = self.origin + '/media/listings/' + AjaxOut.Result.Image[0].ListImageID + '/videoslides/' + AjaxOut.Result.Image[0].ImageName + '"';
        }
        var objImgs = [];
        for (i = 0; i < AjaxOut.Result.RowCount; i++) {
            if (AjaxOut.Result.Image[i].Type == "listing") {
                var src1 = '../../media/listings/' + AjaxOut.Result.Image[i].ListImageID + '/photo/' + AjaxOut.Result.Image[i].ImageName;
            } else if (AjaxOut.Result.Image[i].Type == "listingslide") {
                src1 = '../../media/listings/' + AjaxOut.Result.Image[i].ListImageID + '/videoslides/' + AjaxOut.Result.Image[i].ImageName;
            }
            var imageesgtdh = {
                name: 'img' + i,
                src: src1
            };
            objImgs.push(imageesgtdh);
        }
        ImagestoDisplay = '';        
        ImagestoDisplay = objImgs;

        var audio = new Audio();
        audio.src = $('#my_audio').attr('src');
        $('#my_audio').attr('data-imgcount', AjaxOut.Result.RowCount);
        audio.addEventListener('loadedmetadata', function () {

            timeaudio = audio.duration;

            imgc = AjaxOut.Result.RowCount;
            var imgtime = imgc * 4;

            if (imgtime > timeaudio) {
                timeaudio = imgtime;
            }

            var imgtimetodisplay = timeaudio / imgc;

            ImagesTimetoDisplay = 0;
            ImagesTimetoDisplay = imgtimetodisplay;
            $('#videoid').val(AjaxOut.Result.Image[0].ListImageID);

        });
        $('#mainssimage').html('<img style="height:'+(imgContHeight-2)+'px;object-fit:contain;" src=' + src + '  alt=""/>');
        ImageSrc = '';
        ImageSrc = url;
    }
    else {
        $('#divHtml5').hide();
        $('#divcontrolllermessage').html('<div class="col-md-5"></div><div class="col-md-4"><span>No Video Available</span></div><div class="col-md-3"></div>');
        $('#playbtn').hide();
    }
}