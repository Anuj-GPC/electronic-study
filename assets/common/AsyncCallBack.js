﻿var isSessionLive = true;
function __DoAsyncPostBack(controllerInpData, controllerURL, callBackFunction, errorDirectPage) {
    //var resultAsyncPostBack;
    var functionName = '';
    var postURL = '';

    var urlSplit = controllerURL.split('/');
    functionName = urlSplit[urlSplit.length - 1];

    postURL = controllerURL.substring(0, controllerURL.length - functionName.length - 1);

    $.ajax({
        type: "POST",
        url: postURL,
        data: 'POST_FUNCTION=' + functionName + '&' + 'POST_JSONDATA=' + JSON.stringify(controllerInpData),
        //contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function (xhr, errStatus, errThrown) {
            asyncPostbackError(xhr, errStatus, callBackFunction, errorDirectPage);

        },
        beforeSend: function () {
            showAsyncProgress();
            //isUserInSession();
            if (!isSessionLive)
                return;
        },
        complete: function () { hideAsyncProgress(); },
        success: function (Result, Status) {
            if (Status == "success") {

                //callBackFunction(Result.d);
                callBackFunction(Result);
            }
        }
    });
};

function showAsyncProgress() {
    $('#imcsAsyncProgress').show();
}
function hideAsyncProgress() {
    $('#imcsAsyncProgress').hide();
}

function asyncPostbackError(xhr, errStatus, errThrown, callBackFunction) {
    if (xhr.status == 0) {
        //  alert('Not able to connect to the network.\r\n Please verify the network!');
    }
    else if (xhr.status == 404) {
        // alert('The resouce that you are looking is not found.\r\n Please verify the request page!');
    }
    else if (xhr.status == 500) {
        //   alert('Internal Server Error [500].');
        //  window.location = errorDirectPage;

    }
    else if (exception === 'parsererror') {
        //    alert('Requested JSON parse failed.');
    }
    else if (exception === 'timeout') {
        alert('Time out error.');
    }
    else if (exception === 'abort') {
        alert('Ajax call request aborted.');
    }
    else {
        alert('Uncaught Error.\n' + xhr.responseText);
    }
}


function isUserInSession() {
    var pathname = window.location.pathname;
    if (pathname.toLowerCase().indexOf("startup.aspx") != -1) {
        return;
    }

    $.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/Web/Pages/Login/ValiduserController.aspx/Isuseralive",
        data: '{' + '' + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function (xhr, errStatus, errThrown) {


            isSessionLive = false;


        },
        complete: function () { },
        success: function (Result, Status) {
            if (Status == "success") {
                if (Result.d == false) {
                    isSessionLive = false;

                    window.location = 'http://' + window.location.host + '/Web/Pages/imcsStartup.html';
                } else {
                    return;
                };
            }
        }
    });
}


//Get QueryString Parameters
function __GetQueryStringParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Added by Sachin Thapliyal for allowing Alphanumeric values only for search textbox on 18 june 2013
// Edit by Sachin Thapliyal for allowing Alphanumeric values only for search textbox in FireFox and Chrome on 19 june 2013
function allowAlphaNumeric(event) {
    var key;
    key = (event.keyCode ? event.keyCode : event.which);
    if ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || key == 32 || key == 8 || (key >= 48 && key <= 57)) {
        return true;
    }
    else {
        return false;
    }
}

// Added by Sachin Thapliyal for allowing Alphanumeric values only for search textbox after pasting value on 19 june 2013
function checkOnBlur(obj) {
    var IsSpecialCharacter = "0";
    var value = obj.value.toString();
    for (var i = 0; i < value.length; i++) {
        var key = value.charCodeAt(i);
        if ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || key == 8 || key == 32 || (key >= 48 && key <= 57)) {
            IsSpecialCharacter = "0";
        }
        else {
            IsSpecialCharacter = "1";
            break;
        }

    }
    if (IsSpecialCharacter == "1") {
        alert('Please enter valid Account Nummber.');
        var textbox = document.getElementsByName(obj.name)[0];
        textbox.value = '';
    }
}


function getValue(id) {
    if (($('#' + id).attr('type') == 'checkbox') || ($('#' + id).attr('type') == 'radio')) {
        return $('#' + id).attr('checked');
    }
    else {
        if ($('#' + id).val() != undefined) {
            return $('#' + id).val();
        }
        else {
            return "";
        }
    }
}
function GetAjaxObjectSync(svcname, params, method) {
    $.ajax({
        type: "POST",
        url: svcname,
        contentType: "application/json; charset=utf-8",
        data: '{' + params + '}',
        dataType: "json",
        async: false,
        success: function (Result, Status) {
            // Process the Result
            if (Status == "success") {
                AjaxResult = Result;
                eval(method);
            }
            else {
                alert("Unable to Load Object");
            }
        }
    });
}
this.bindDropDownSync = function (ddid, svcname, params, textfield, valuefield, addclearoption, nxtfunc) {
    try {
        if (svcname != "" && ddid != null) {
            DropDownsBinding++;
            $.ajax({
                type: "POST",
                url: svcname,
                contentType: "application/json; charset=utf-8",
                data: "{" + params + "}",
                dataType: "json",
                async: false,
                error: function (xml, err, str) {
                    DropDownsBinding--;
                },
                success: function (Result, Status) {
                    // Process the Result
                    if (Status == "success") {
                        //
                        var drp = document.getElementById(ddid);
                        if (drp != null) {
                            drp.options.length = 0;
                            if (addclearoption) {
                                drp.options[drp.options.length] = new Option("undefined", "");
                            }
                            $.each(Result.d, function (ky, val) {
                                drp.options[drp.options.length] = new Option(val[textfield], val[valuefield]);
                                //drp.options[drp.options.length] = new Option(val[textfield], val[textfield]);
                            }
                            );
                        }
                        DropDownsBinding--;
                        eval(nxtfunc);
                    }
                    else {
                        DropDownsBinding--;
                        alert("An error has occured " + status);
                        // it has failed for some reason
                    }
                }
            });
        }
    }
    catch (bindingerror) {
        DropDownsBinding--;
    }
};

this.bindListBoxSync = function (lstboxid, svcname, params) {
    try {
        if (svcname != "" && lstboxid != null) {
            DropDownsBinding++;
            $.ajax({
                type: "POST",
                url: svcname,
                contentType: "application/json; charset=utf-8",
                data: "{" + params + "}",
                dataType: "json",
                async: false,
                error: function (xml, err, str) {
                    DropDownsBinding--;
                },
                success: function (Result, Status) {
                    // Process the Result
                    if (Status == "success") {
                        var drp = document.getElementById(lstboxid);
                        if (drp != null) {
                            drp.options.length = 0;
                            $.each(Result.d, function (ky, val) {
                                drp.options[drp.options.length] = new Option(Result.d[ky], Result.d[ky]);
                            });
                        }
                        DropDownsBinding--;
                    }
                    else {
                        DropDownsBinding--;
                        alert("An error has occured " + status);
                        // it has failed for some reason
                    }
                }
            });
        }
    }
    catch (bindingerror) {
        DropDownsBinding--;
    }
};

function AjaxInputObject() {
    var ajaxInput = { command: "", sender: "", message: "", keyValue: [] };
    return ajaxInput;
};

function AjaxOutputObject() {
    var ajaxOutput = {
        command: "",
        commandArgs: [],
        message: "",
        resultData: [],
        sender: "",
        success: ""
    }
    return ajaxOutput;
};

function CheckNull(str) {
    if (str == null || str == '0')
        return '';
    else
        return str;
};

function CleanString(str) {
    //Will add more here
    str = str.replace(/'/g, "\'");
    str = str.replace(/"/g,  "\'");
    return str;
};

function SmallDate(date) {
    var newdate = new Date(date);

    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();

    if (mm.toString().length == 1) {
        mm = '0' + mm;
    }

    if (dd.toString().length == 1) {
        dd = '0' + dd;
    }

    var someFormattedDate = y + '-' + mm + '-' + dd;
    return someFormattedDate;
}