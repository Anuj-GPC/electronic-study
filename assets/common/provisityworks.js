provisityWorks = {
  /*---------------------
    --primaryColor: #9c27b0;
    --secondaryColor:#00bcd4;
    --bodyBackground:#EEEEEE;
    -------------------------------*/
  Initials: "",

  setTheme: function (franchise) {
    var primaryColor = "#3E3E3F";
    var secondaryColor = "#534E49"; //#534E49
    var bodyBackground = "#EEEEEE";
    var navBarColor = "#009775";
    var boxShadow = "rgba(0,0,0,.28)";

    //color choices for fwg
    if (franchise == "FW") {
      primaryColor = "#d25e00"; //light blue
      secondaryColor = "#534E49"; //; /*'#666159';*/
      bodyBackground = "#E9E9E1";
      navBarColor = "#999999";
      boxShadow = "rgba(0,0,0,.28)";
    }

    //color choices for CB
    if (franchise == "CB") {
      primaryColor = "#004990"; //light blue
      secondaryColor = "#004990"; //; /*'#666159';*/
      bodyBackground = "#EEEEEE";
      navBarColor = "#999999";
      boxShadow = "rgba(0,0,0,.28)";
    }

    if(franchise == "E"){
      primaryColor = "#3e3e3f";
      secondaryColor = "#018A99"; 
      bodyBackground = "#eeeeee";
      navBarColor = "#018A99"; 
      boxShadow = "rgba(0,0,0,.28)";
    }

    if(franchise == "EX"){
      primaryColor = "#3e3e3f";
      secondaryColor = "#018A99"; 
      bodyBackground = "#eeeeee";
      navBarColor = "#018A99"; 
      boxShadow = "rgba(0,0,0,.28)";
    }

    const provThemeElement = document.documentElement;
    provThemeElement.style.setProperty("--primaryColor", primaryColor);
    provThemeElement.style.setProperty("--secondaryColor", secondaryColor);
    provThemeElement.style.setProperty("--bodyBackground", bodyBackground);
    provThemeElement.style.setProperty("--navBarColor", navBarColor);
    // provThemeElement.style.setProperty('--navBarColor',boxShadow);
  }
};
//--------------------- executions

var FranIni = localStorage.getItem("FranIni");
Initials = FranIni;

var url = window.location.hostname;

if (FranIni != null) {
  switch (FranIni) {
    case "FW":
      provisityWorks.setTheme("FW");
      $('#dynamicprovisitylogo').attr('src', '../assets/images/Logo/FirstWeberLogo.jpg');
      break;
    case "CB":
      provisityWorks.setTheme(FranIni);
      $('#dynamicprovisitylogo').attr('src', '../assets/images/Logo/CB-Logo.jpg');
      break;
    case "cb.imcstech.com":
      provisityWorks.setTheme("CB");
      break;
    case "E"  :
      provisityWorks.setTheme(FranIni);
      break;
    case "EX"  :
    $('#dynamicprovisitylogo').attr('src', '../assets/images/Logo/EXITRealty.png');
      provisityWorks.setTheme(FranIni);
      break;  
    default:
      provisityWorks.setTheme("");
      $('#dynamicprovisitylogo').attr('src', '../assets/images/Logo/ProvisityLogo.jpg');
  }
} else {
  switch (url) {
    case "localhost":
      provisityWorks.setTheme("FW");
      break;
    case "fwg.imcstech.com":
      provisityWorks.setTheme("FW");
      break;
    default:
      provisityWorks.setTheme("");
  }
}

function LoadLogo() {
  if (Initials != null) {
    switch (Initials) {
      case "FW":
        $('#dynamicprovisitylogo').attr('src', '../assets/images/Logo/FirstWeberLogo.jpg');
        break;
      case "CB":
        $('#dynamicprovisitylogo').attr('src', '../assets/images/Logo/CB-Logo.jpg');
        break;
      case "E":
        $('#dynamicprovisitylogo').attr('src', '../assets/images/Logo/EXITRealty.png');
        break;
      case "EX":
        $('#dynamicprovisitylogo').attr('src', '../assets/images/Logo/EXITRealty.png');
        break;
      default:
        $('#dynamicprovisitylogo').attr('src', '../assets/images/Logo/ProvisityLogo.jpg');
    }
  }
}

function GetCurrentPage() {
  var keylist = {};
  keylist["/web/html/myteam.html"] = "liMyTeam";
  keylist["/web/html/listing.html"] = "liListing";
  keylist["/web/html/user.html"] = "liUser";
  keylist["/web/html/agency.html"] = "liAgency";
  keylist["/web/fb/marketingmaterial.php"] = "liMarketingMaterial";
  keylist["/web/html/officeadmin.html"] = "liOfficeAdmin";
  keylist["/web/html/reports.html"] = "liReports";

  keylist["/web/html/managefrenchies.html"] = "liManageFrenchies";
  keylist["/web/html/manageoffices.html"] = "liManageOffices";
  keylist["/web/html/alllistings.html"] = "liAllListings";

  $('#' + keylist[window.location.pathname]).addClass("active");

  if (window.location.pathname == "/web/html/officeadmin.html") {
    $('#divSelect').show();
  }
}

//call loader include function
function includeloaderHTML() {
  var z, i, elmnt, file, xhttp;
  /*loop through a collection of all HTML elements:*/
  z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
    file = elmnt.getAttribute("data-include-html");
    if (file) {
      console.log(file);
      /*make an HTTP request using the attribute value as the file name:*/
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function () {
        if (this.readyState == 4) {
          if (this.status == 200) { elmnt.innerHTML = this.responseText; }
          if (this.status == 404) { elmnt.innerHTML = "Page not found."; }
          /*remove the attribute, and call this function once more:*/
          elmnt.removeAttribute("data-include-html");
          includeloaderHTML();
        }
      }
      xhttp.open("GET", file, true);
      xhttp.send();
      /*exit the function:*/
      return;
    }
  }
};
