Paging = {
    totalNumberOfPage: 0,
    currentpageindex: {},
    lastpageindex: 10,
    callpage: {},
    callBack: {},
    currentactiveid:'',

    managepageobj:{},
preparemandatorydesign:function(id){
    $('.'+id).append('<span id="'+id+'totalpageorder"></span)');
    $("#"+id).append('<a href="#" parentid="'+id+'" id="'+id+'prevpaging" class="prevpaging" style="display:none;" onclick="Paging.onclickpreviousButton(this.id)">PREVIOUS</a>');
    $("#"+id).append('<a href="#" parentid="'+id+'"  id="'+id+'nextpaging" class="nextpaging" onclick="Paging.onclicknextButton(this.id)">NEXT</a>');

},
    paging: function (TotalRecords, pageSize,idtoappend) {
        Paging.callpage[idtoappend] =false;
        Paging.currentactiveid=idtoappend;
        $("#"+idtoappend).html("");
        var pageCount = TotalRecords / pageSize;
        pageCount = Math.ceil(pageCount);
        if (pageCount <= 1) {
            $('#'+idtoappend+'nextpaging').hide();
            pageCount = 1;
        }
        Paging.totalNumberOfPage = pageCount;
        Paging.managepageobj[idtoappend+"totalnumberofpage"]=pageCount;
        Paging.preparemandatorydesign(idtoappend);
        $('#'+idtoappend+'totalpageorder').html('Page 1 of ' + Paging.totalNumberOfPage);

      

        if (Paging.totalNumberOfPage < Paging.lastpageindex) {
            Paging.preparePageicon(0, Paging.totalNumberOfPage);
        } else {
            Paging.preparePageicon(0, Paging.lastpageindex);
        }

        if (Paging.totalNumberOfPage == 1) {
            $("#"+idtoappend).hide();
        }
        else{
            $("#"+idtoappend).show();
        }

        $("#"+idtoappend+"lstPager1").addClass("current");
    },

    preparePageicon: function (startindex, endindex) {
        
        try {
            for (var i = startindex; i < endindex; i++) {
                $("#"+  Paging.currentactiveid+"nextpaging").before(
                    '<li id="li'+Paging.currentactiveid+ (i + 1) +'" style="padding: 0.5%;" currentActivepageing="'+Paging.currentactiveid+'" onclick="Paging.onclickpageicon('+ (i + 1) + ',this.id)"><a id="'+Paging.currentactiveid+'lstPager' + (i + 1) + '" href="#">' + (i + 1) + "</a></li> "
                );
            }
        }
        catch (ex) {
        }
    },

    onclicknextButton: function (id) {
        Paging.currentactiveid=$('#'+id).attr('parentid');
        Paging.currentpageindex[ Paging.currentactiveid]= Paging.currentpageindex[ Paging.currentactiveid] + 1
        Paging.onclickpageicon(Paging.currentpageindex[ Paging.currentactiveid],'li'+Paging.currentactiveid+Paging.currentpageindex[ Paging.currentactiveid]);
    },

    onclickpreviousButton: function (id) {
        Paging.currentactiveid=$('#'+id).attr('parentid');
        Paging.currentpageindex[ Paging.currentactiveid] = Paging.currentpageindex[ Paging.currentactiveid]- 1
        Paging.onclickpageicon(Paging.currentpageindex[ Paging.currentactiveid],'li'+Paging.currentactiveid+Paging.currentpageindex[ Paging.currentactiveid]);
    },

    onclickpageicon: function (pagenumber,currentidj) {
        Paging.currentactiveid=$('#'+currentidj).attr('currentActivepageing');
        var idtoappend=Paging.currentactiveid;
        $('#'+idtoappend+'totalpageorder').html('Page ' + pagenumber + ' of ' +Paging.managepageobj[idtoappend+"totalnumberofpage"]);
        if (pagenumber > 1) {
            $('#'+idtoappend+'prevpaging').show();
      
        }
        else {
         
            $('#'+idtoappend+'prevpaging').hide();
        }

        if (pagenumber ==Paging.managepageobj[idtoappend+"totalnumberofpage"]) {
            $('#'+idtoappend+'nextpaging').hide();
           // $("#nextpagingaction").hide();
        } else {
            $('#'+idtoappend+'nextpaging').show();
            // $("#nextpagingaction").show();
        }
        $('#'+idtoappend+' li').remove();

        Paging.currentpageindex[idtoappend] = pagenumber;

        var lastindex = pagenumber + 5;
        if (lastindex >= Paging.managepageobj[idtoappend+"totalnumberofpage"]) {
            lastindex =Paging.managepageobj[idtoappend+"totalnumberofpage"];
        }

        var startindex = lastindex - Paging.lastpageindex;

        if (startindex < 0) {
            startindex = 1;
        }

        if (pagenumber > 5) {
            Paging.preparePageicon(startindex, lastindex);
        }
        else if (Paging.managepageobj[idtoappend+"totalnumberofpage"]< Paging.lastpageindex) {
            Paging.preparePageicon(0,Paging.managepageobj[idtoappend+"totalnumberofpage"]);
        } else {
            Paging.preparePageicon(0, Paging.lastpageindex);
        }
        $("#"+idtoappend+"lstPager" + (pagenumber)).addClass("current");
        Paging.callBack[idtoappend](pagenumber);
 
    },

   
}