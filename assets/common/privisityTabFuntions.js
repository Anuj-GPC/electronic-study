jQuery(document).ready(function () {
    jQuery('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        localStorage.setItem('activeTab', jQuery(e.target).attr('href'));
    });

    // Here, save the index to which the tab corresponds. You can see it 
    // in the chrome dev tool.
    var activeTab = localStorage.getItem('activeTab');

    // In the console you will be shown the tab where you made the last 
    // click and the save to "activeTab". I leave the console for you to 
    // see. And when you refresh the browser, the last one where you 
    // clicked will be active.
    console.log(activeTab);

    if (activeTab) {
        jQuery('a[href="' + activeTab + '"]').tab('show');
    }
});

//        var slideIndex = 0;
//
//        var myVar;




window.confirm = function (message, cb,id) {
    //        alert('hi');
    $("#"+id).modal('show');
    $("#"+ id +" .modal-body p").html(message);
    $("#confirmYes").on("click", function (userChoice) {
        cb(true); //true or false - your jquery plugin will supply this value 
    });

}




//text area auto resize starrt

$(function () {
    //  changes mouse cursor when highlighting loawer right of box
    $("textarea").mousemove(function (e) {
        var myPos = $(this).offset();
        myPos.bottom = $(this).offset().top + $(this).outerHeight();
        myPos.right = $(this).offset().left + $(this).outerWidth();

        if (myPos.bottom > e.pageY && e.pageY > myPos.bottom - 16 && myPos.right > e.pageX && e.pageX > myPos.right - 16) {
            $(this).css({
                cursor: "nw-resize"
            });
        } else {
            $(this).css({
                cursor: ""
            });
        }
    })
        //  the following simple make the textbox "Auto-Expand" as it is typed in
        .keyup(function (e) {
            //  this if statement checks to see if backspace or delete was pressed, if so, it resets the height of the box so it can be resized properly
            if (e.which == 8 || e.which == 46) {
                $(this).height(parseFloat($(this).css("min-height")) != 0 ? parseFloat($(this).css("min-height")) : parseFloat($(this).css("font-size")));
            }
            //  the following will help the text expand as typing takes place
            while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                $(this).height($(this).height() + 1);
            };
        });
});

//$('#about_me').focus(); 

function callonfocusAboutme() {
    $("#about_me").focus();
    var that = $('#about_me').get(0);
    while ($(that).outerHeight() < that.scrollHeight + parseFloat($(that).css("borderTopWidth")) + parseFloat($(that).css("borderBottomWidth"))) {
        $(that).height($(that).height() + 1);

    }
    $("#about_me").blur();
}

//auto resize end

//mob tab fixed
$(document).ready(function () {
    try {
        //        var bodyh = document.body.clientHeight;
        var windowh = window.innerHeight;
        //    var winheight= window.screen.availHeight;//document.body.clientHeight;// $(window).height();
        $('.fixedContainer').css('margin-top', windowh - 43);
        if ($(window).width() > '750') {
            $('#ulAccountToggle').addClass('card');
            $('#ulAccountToggle').removeClass('dropdown-menu');
            $('#ulAccountToggle').attr('style', 'display: none;    position: relative;    list-style: none;    color: black;    padding: 11px;    margin-top: 0px;    cursor: pointer;');
            $('#aAccounttoggle').attr('style', 'float:right;');
            $('#liAccounttoggle').attr('style', 'width:220px;');
        }
        localStorage.setItem('searchkey', '');
        //        $('.wrapper').addClass('wrap_per');
    } catch (ex) {

    }

});

$(document).scroll(function () {
    try {
        var windowh = window.innerHeight;
        //    var winheight= window.screen.availHeight;//document.body.clientHeight;// $(window).height();
        $('.fixedContainer').css('margin-top', windowh - 43);
        //        $('.wrapper').addClass('wrap_per');
    } catch (ex) {

    }
});
$(window).resize(function () {
    try {
        var windowh = window.innerHeight;
        //    var winheight= window.screen.availHeight;//document.body.clientHeight;// $(window).height();
        $('.fixedContainer').css('margin-top', windowh - 43);
        //        $('.wrapper').addClass('wrap_per');
    } catch (ex) {

    }
});